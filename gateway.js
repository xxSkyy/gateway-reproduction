"use strict"
const Fastify = require("fastify")
const mercurius = require("mercurius")
const gateway = Fastify()

gateway.register(mercurius, {
  gateway: {
    services: [
      {
        name: "user",
        url: "http://localhost:3001/graphql",
      },
      // {
      //   name: "review",
      //   url: "http://localhost:3002/graphql",
      // },
    ],
    pollingInterval: 2000,
  },
})

gateway.listen(3000)
console.log("Running gateway!")
