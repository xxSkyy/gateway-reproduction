"use strict"

const Fastify = require("fastify")
const mercurius = require("mercurius")
const { makeExecutableSchema } = require("@graphql-tools/schema")

const users = {
  1: {
    id: "1",
    name: "John",
    username: "@john",
  },
  2: {
    id: "2",
    name: "Jane",
    username: "@jane",
  },
}

const app = Fastify()
const schema = `
type Query {
    version: String
    me: User
 }

 type User {
  id: ID!
  name: String
  username: String
 }
`

const resolvers = {
  Query: {
    version: () => "1.0.0",
    me: () => {
      return users["1"]
    },
  },
  User: {
    __resolveReference: (source, args, context, info) => {
      return users[source.id]
    },
  },
}

const loaders = {
  User: {
    __resolveReference: (queries, context) => {
      // This is a bulk query to the database
      return queries.map(({ obj }) => users[obj.id])
    },
  },
}

const init = async () => {
  const workingSchema = schema
  const notWorkingSchema = makeExecutableSchema({ typeDefs: schema, resolvers })

  app.register(mercurius, {
    graphiql: "graphiql",
    schema: notWorkingSchema,
    // resolvers,
    // loaders,
    jit: 1,
    federationMetadata: true,
  })

  app.listen(3001)
  console.log("Running user microservice!")
}

init()
