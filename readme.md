# Repro

Steps:

`yarn`

`node user`

`node gateway`

throws error.

Replace line `63`

`schema: notWorkingSchema,`
to  
`schema: workingSchema,`

and uncomment `resolvers,` and `loaders,`

that actually is schema not loaded by `graphql-tools` and gateway works.

`graphql-tools` version doesn't matter in this case, mercurius works well but gateway doesn't load schema.